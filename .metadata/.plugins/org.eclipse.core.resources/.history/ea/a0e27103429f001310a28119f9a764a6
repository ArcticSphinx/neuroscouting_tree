import java.util.Scanner;

public class Main {

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//number of levels the tree has. By default, this is 0.
		int treeSize = 0;
		
		//The tree is an array of nodes
		Node[] nodeTree;
		
		//Make a scanner to take user input
		Scanner userInput = new Scanner(System.in);
		
		//Keep running this loop until a valid number is entered
		while(treeSize == 0)
		{
			//prompt for input
			System.out.println("How many levels would you like this tree to have?");
			
			//store input in string
			String uInp = userInput.nextLine();
			
			//try to parse string to an int
			try
			{
				//if this is an integer, it will become the new treeSize.
				treeSize = Integer.parseInt(uInp);
			} catch(NumberFormatException e)
			{
				//Give the user a mild scolding if they entered something that isn't an integer.
				System.out.println("Please enter a positive integer. \n");
			}
			
			//don't let the user create a tree with negative elements.
			if (treeSize < 0)
			{
				//This is a negative number. Set to 0 and loop back.
				treeSize = 0;
				//Emphasis on POSITIVE. Trees shouldn't grow backwards.
				System.out.println("Please enter a positive integer. \n");
			}
			
		}
	
		//I just want to see what happens right now
		System.out.println("Tree Size: " + treeSize);
		
		//total number of nodes in nodeTree
		int nodeNum = 0;
		
		//set size of nodeTree
		for (int i = treeSize; i > 0; i--)
		{
			//The total number of nodes is going to be 2^n + 2^n-1 + 2^n-2...etc., where 'n' is treeSize
			nodeNum += Math.pow(2, (i - 1));
		}
		
		//set the size of nodeTree to nodeNum
		nodeTree = new Node[nodeNum];
		
		//proof that the right number of nodes are being accounted for in the array
		//System.out.println("\nnodeNum: " + nodeNum);
		
		//There is nothing here, but at least let the user know that he/she did something
		if(treeSize == 0)
		{
			System.out.println("This tree is empty.");
		}
		
		//loop through this for each node in the tree
		for (int i = 0; i < nodeNum; i++)
		{
			//let's just get the first node out of the way now.
			if(i == 0)
			{
				//This is the root node
				Node n = new Node(1);
				nodeTree[i] = n;
				
				//ignoring for now
				//only bother to do this if there is more than 1 node...
				/*
				if (treeSize > 1)
				{
					//create left child
					Node lc = new Node(nodeTree[i].nodeVal);
					nodeTree[i].leftChild = lc;
					
					//create right child
					Node rc = new Node(nodeTree[i].nodeVal);
					nodeTree[i].rightChild = rc;
					
					//children are siblings
					nodeTree[i].leftChild.rightSibling = rc;
					nodeTree[i].rightChild.leftSibling = lc;
					
					//add children to nodeTree
					int child = i + 1; //child is the index of the array that the child is stored in
					nodeTree[child] = lc;
					
					child++; 
					nodeTree[child] = rc;
					
				}
				*/	
			}//end first node
			
			//for every other node
			else
			{
				//create node and determine relationships
				Node n = new Node();
				
				//this becomes the right sibling of the previous node if it doesn't have one already and isn't at the edge of the tree
				if(nodeTree[i - 1].rightSibling == null &&  nodeTree[i - 1].parent.rightSibling != null)
				{
					//don't do this if the only other node is the root node
					if((i - 1) != 0)
					{
						//make this the right sibling of the last node
						nodeTree[i - 1].rightSibling = n;
						
						//make the last node this node's left sibling
						n.leftSibling = nodeTree[i - 1];
						
						//make this node's parent the left sibling's parent
						n.parent = n.leftSibling.parent;
					}
				}
				//if no more right siblings can be made, we go down one level on the tree
				else 
				{
					//now we look for nodes without left siblings
					for(int j = 1; j < nodeNum; j++)
					{
						if(nodeTree[j].leftChild == null)
						{
							//set child-less node's left child to n
							nodeTree[j].leftChild = n;
							
							//make the node at the index of j, n's parent
							n.parent = nodeTree[j];
							
							break; //break out of the for loop
						}
					}//end of j
				}//end of nested else
				
				//now, set value of new node to its parent's nodeVal
				n.nodeVal = n.parent.nodeVal;
				
				//add parent's left sibling's value, if applicable
				if(n.parent.leftSibling != null)
				{
					n.nodeVal += n.parent.leftSibling.nodeVal;
				}
				
				//...and add parent's right sibling's value, if applicable
				if(n.parent.rightSibling != null)
				{
					n.nodeVal += n.parent.rightSibling.nodeVal;
				}
				
				//store n in nodeTree
				nodeTree[i] = n;
				
			}//end of non-root node creation
			
		}
		
		//*
		//print each node
		for (int i = 0; i < nodeNum; i++)
		{
			//only bother to do this if the nodes in the tree aren't null
			if(nodeTree[i] != null)
			{
				System.out.println("\nNode " + (i + 1) + ": " + nodeTree[i].nodeVal);
			}
		}
		//*/
		
		
		userInput.close(); //close scanner
	}

}
