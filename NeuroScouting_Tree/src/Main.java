import java.util.Scanner;

public class Main {

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//number of levels the tree has. By default, this is 0.
		int treeSize = 0;
		int treeLevel = 1;
		
		//The tree is an array of nodes
		Node[] nodeTree;
		
		//Make a scanner to take user input
		Scanner userInput = new Scanner(System.in);
		
		//Keep running this loop until a valid number is entered
		while(treeSize == 0)
		{
			//prompt for input
			System.out.println("How many levels would you like this tree to have?");
			
			//store input in string
			String uInp = userInput.nextLine();
			
			//try to parse string to an int
			try
			{
				//if this is an integer, it will become the new treeSize.
				treeSize = Integer.parseInt(uInp);
			} catch(NumberFormatException e)
			{
				//Give the user a mild scolding if they entered something that isn't an integer.
				System.out.println("Please enter a positive integer. \n");
			}
			
			//don't let the user create a tree with negative elements.
			if (treeSize < 0)
			{
				//This is a negative number. Set to 0 and loop back.
				treeSize = 0;
				//Emphasis on POSITIVE. Trees shouldn't grow backwards.
				System.out.println("Please enter a positive integer. \n");
			}
			
		}
	
		//I just want to see what happens right now
		System.out.println("Tree Size: " + treeSize);
		
		//total number of nodes in nodeTree
		int nodeNum = 0;
		
		//set size of nodeTree
		for (int i = treeSize; i > 0; i--)
		{
			//The total number of nodes is going to be 2^n + 2^n-1 + 2^n-2...etc., where 'n' is treeSize
			nodeNum += Math.pow(2, (i - 1));
		}
		
		//set the size of nodeTree to nodeNum
		nodeTree = new Node[nodeNum];
		
		//proof that the right number of nodes are being accounted for in the array
		//System.out.println("\nnodeNum: " + nodeNum);
		
		//There is nothing here, but at least let the user know that he/she did something
		if(treeSize == 0)
		{
			System.out.println("This tree is empty.");
		}
		
		//loop through this for each node in the tree
		for (int i = 0; i < nodeNum; i++)
		{
			//used this for testing -- Don't need it anymore. It's just taking up space
			//System.out.println("i = " + i); //double-checking i values -- should skip 1 and 2
		
			//let's just get the first node out of the way now.
			if(i == 0)
			{
				//This is the root node
				Node n = new Node(1);
				nodeTree[i] = n;
								
				//ignoring for now
				//only bother to do this if there is more than 1 node...
				//*
				if (treeSize > 1)
				{
					//create left child
					Node lc = new Node(nodeTree[i].nodeVal);
					nodeTree[i].leftChild = lc;
					lc.parent = nodeTree[i];
					
					//create right child
					Node rc = new Node(nodeTree[i].nodeVal);
					nodeTree[i].rightChild = rc;
					rc.parent = nodeTree[i];
					
					//children are siblings
					nodeTree[i].leftChild.rightSibling = rc;
					nodeTree[i].rightChild.leftSibling = lc;
					
					//add children to nodeTree
					int child = i + 1; //child is the index of the array that the child is stored in
					nodeTree[child] = lc;
					
					//This line was used for testing--it is no longer needed
					//System.out.println("child = " + child);
					
					child++; 
					nodeTree[child] = rc;
					
					//System.out.println("child = " + child);
					
					//i becomes child
					i = child;
					
				}
				//*/	
			}//end root node
			
			//for every other node
			else
			{
				//create node and determine relationships
				Node n = new Node();
				
				//store n in nodeTree
				nodeTree[i] = n;
				
				//this becomes the right sibling of the previous node if it doesn't have one already and isn't at the edge of the tree
				//if i%2 is 1, the number is even, which means that it is the left child of a node
				if((i % 2) == 1)
				{
					
					//loop through entire tree, looking for nodes without left children
					for(int j = 0; j < nodeNum; j++)
					{
						//we have found one
						if(nodeTree[j].leftChild.nodeVal == -1) // checking for nodes without left children
						{
							//establish parental relationship
							nodeTree[i].parent = nodeTree[j]; //the parent of this node becomes the first node without a left child
							nodeTree[j].leftChild = nodeTree[i]; //...and this node becomes the left child of its parent
							
							//give this node a value
							//start by making sure its parent is giving the correct value
							//it is a left child, so: 
							nodeTree[i].nodeVal = nodeTree[j].nodeVal; //set value to parent's value
							//And, if applicable...
							if(nodeTree[j].leftSibling.nodeVal != -1)
							{
								nodeTree[i].nodeVal += nodeTree[j].leftSibling.nodeVal; //...add the parent's leftSibling's value
							}
							
							break; //break out of the loop
							
						}
					}
					
					//if this node's grandparent is the same as its predecessor...
					if(nodeTree[i].parent.parent == nodeTree[i-1].parent.parent)
					{
						//they are siblings
						nodeTree[i].leftSibling = nodeTree[i-1];
						nodeTree[i-1].rightSibling = nodeTree[i];
					}
					
				}
				//since i%2 != 1, it must be 0, which means the node is a right child
				else 
				{
					//loop through entire tree, looking for nodes without right children
					for(int h = 0; h < nodeNum; h++)
					{
						//we have found one
						if(nodeTree[h].rightChild.nodeVal == -1) // checking for nodes without right children
						{
							//establish parental relationship
							nodeTree[i].parent = nodeTree[h]; //the parent of this node becomes the first node without a right child
							nodeTree[h].rightChild = nodeTree[i]; //...and this node becomes the right child of its parent
							
							//give this node a value
							//start by making sure its parent is giving the correct value
							//it is a right child, so: 
							nodeTree[i].nodeVal = nodeTree[h].nodeVal; //set value to parent's value
							//And, if applicable...
							if(nodeTree[h].rightSibling.nodeVal != -1)
							{
								nodeTree[i].nodeVal += nodeTree[h].rightSibling.nodeVal; //...add the parent's rightSibling's value
							}
							
							//make this the right sibling of its predecessor, and vice-versa
							nodeTree[i].leftSibling = nodeTree[i-1];
							nodeTree[i-1].rightSibling = nodeTree[i];
							
							break; //break out of the loop
						}
					}
					
				
				}
				
								
			}//end of non-root node creation
			
		}
		
		//*
		//print each node
		for (int i = 0; i < nodeNum; i++)
		{
			//holds current treelevel
			int tL = treeLevel;
		
			if (i >= Math.pow(2,treeLevel))
			{
				treeLevel++;
			}
			
			//always print the first node
			if(i < Math.pow(2,treeLevel))
			{
				System.out.print("\n");				
			} 
			
			//print node value
			if(i == 0)
			{
				//do this if it is the root node, only
				System.out.println(nodeTree[i].nodeVal);
			}
			else
			{
				System.out.print(nodeTree[i].nodeVal);
			}
			
			//print the node
			//System.out.println("\nNode " + (i + 1) + ": " + nodeTree[i].nodeVal);
			
		}
		//*/
		
		
		userInput.close(); //close scanner
	}

}
