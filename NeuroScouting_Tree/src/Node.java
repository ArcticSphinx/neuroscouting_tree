//Node for the tree
public class Node 
{

	//value in this node
	public int nodeVal;
	
	//parent node
	Node parent;
	
	//left and right children of this node
	Node leftChild; 
	Node rightChild;
	
	//left and right siblings of this node
	Node leftSibling;
	Node rightSibling;
	
	//constructor for root node
	public Node(int val)
	{
		nodeVal = val;
		
		if (val != -1)
		{
			leftChild = new Node(-1);
			rightChild = new Node(-1);
			leftSibling = new Node(-1);
			rightSibling = new Node(-1);
		}
		
	}
	
	//empty constructor for other nodes
	public Node()
	{
		//setting nodes to -1 by default should prevent null pointer exceptions
		leftChild = new Node(-1);
		rightChild = new Node(-1);
		leftSibling = new Node(-1);
		rightSibling = new Node(-1);
		
	}
	
	//check for null node without checking for a null node
	public boolean isEmpty(Node n)
	{
		if(n != null)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
}
